import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MarketPage extends StatefulWidget {
  MarketPage({Key key}) : super(key: key);

  @override
  _MarketPageState createState() => _MarketPageState();
}

class _MarketPageState extends State<MarketPage> {
  // rotation playing
  Widget _swiperWidget() {
    //local images/files in the image folder, will be removed when db is ready
    List<Map> imgList = [
      {'url': 'images/1.png'},
      {'url': 'images/2.png'},
      {'url': 'images/3.png'},
    ];
    return Container(
        child: AspectRatio(
            aspectRatio: 2 / 1,
            child: Swiper(
              itemBuilder: (BuildContext context, int index) {
                // Need to change to new Image.network from Image.asset when db is ready
                return new Image.asset(
                  imgList[index]['url'],
                  fit: BoxFit.fill,
                );
              },
              itemCount: imgList.length,
              pagination: new SwiperPagination(),
              autoplay: true,
            )));
  }

  Widget _titleWidget(value) {
    return Container(
      height: ScreenUtil.getInstance().setHeight(32),
      margin: EdgeInsets.only(left: ScreenUtil.getInstance().setWidth(20)),
      padding: EdgeInsets.only(left: ScreenUtil.getInstance().setWidth(20)),
      decoration: BoxDecoration(
          border: Border(
              left: BorderSide(
                  color: Colors.red,
                  width: ScreenUtil.getInstance().setWidth(10)))),
      child: Text(
        value,
        style: TextStyle(color: Colors.black54),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil(width: 750, height: 1334)..init(context);
    return ListView(
      children: <Widget>[
        _swiperWidget(),
        SizedBox(height: 10),
        _titleWidget('Popular')
      ],
    );
  }
}
